//
//  WinnerViewController.m
//  GameDayXtra
//
//  Created by Macmini on 22/06/15.
//  Copyright (c) 2015 Macmini. All rights reserved.
//

#import "WinnerViewController.h"
#import "TeamPlayerViewController.h"
#import "Sports.h"


@interface WinnerViewController ()

@end

@implementation WinnerViewController

- (void)viewDidLoad
{
    [self setupWinnerUIGDX];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden=TRUE;
}

-(void)setupWinnerUIGDX
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    
    searchWinnerArray=[[NSMutableArray alloc] init];
    searching=NO;
    letsUserSelectRow=YES;
    
    
    if (_globalGDXData.internetCheck == YES) {
        
        [self.indicatorSportProfile startAnimating];
        _gdxSync.delegate=self;
        [_gdxSync getSportsMethod];
        
    }
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    [_globalGDXData setSearchBarFontAndTextColor:self];// For change SearchBar text color and font
    
    [_searchBar setBackgroundImage:[UIImage new]];
}


-(void)getSportsProfileResponseData :(NSDictionary *)sportsDict error:(NSError *)aError
{
    if ([[[sportsDict objectForKey:@"status"] objectForKey:@"code"] intValue] == kSuccessStatus) {
        
        sports=[Sports new];
        winnerArray=[[NSMutableArray alloc] init];
        
        for (int i=0; i <[[sportsDict objectForKey:@"resp"] count] ; i++)
        {
            sports =[sports initWithAttributes:[[sportsDict objectForKey:@"resp"] objectAtIndex:i]];
            [winnerArray addObject:sports];
        }
        
        [_tbl_winner reloadData];
        
    } else {
        [_globalGDXData showAlertTitle:kAlertFailed message:[[sportsDict objectForKey:@"status"] objectForKey:@"message"]];
    }
    [self.indicatorSportProfile stopAnimating];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;   // called when text changes (including clear)
{
    [searchWinnerArray removeAllObjects];
    
    if([searchText length] > 0) {
        searching = YES;
        letsUserSelectRow = YES;
        [self searchtableview];
    } else {
        searching = NO;
        letsUserSelectRow= YES;
        [_searchBar resignFirstResponder];
    }
    
    [_tbl_winner reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
    [_searchBar resignFirstResponder];
}
-(void)searchtableview
{
    NSString *sportsearchTxt =[_searchBar.text uppercaseString];
    
    @try {
        
        for(int l = 0; l < [winnerArray count]; l++)
        {
            @autoreleasepool {
                
                sports=[winnerArray objectAtIndex:l];
                
                NSString *tempstr = [sports.SportName uppercaseString];
                NSRange rngstr = [tempstr rangeOfString:sportsearchTxt options:NSCaseInsensitiveSearch];
                
                if(rngstr.length > 0)
                {
                    [searchWinnerArray addObject:[winnerArray objectAtIndex:l]];
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
    
    [_tbl_winner reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searching){
        return [searchWinnerArray count];
    }else {
        return [winnerArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell=nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row % 2 == 0){
            cell.backgroundColor =kColorCell;
        }else{
            cell.backgroundColor =kColorWhite;
        }
        
        @autoreleasepool {
            
            UILabel *lblSportsName = [[UILabel alloc] init];
            lblSportsName.textColor=kColorBlack;
            lblSportsName.backgroundColor=kColorClear;
            
            if(searching && [_searchBar.text length] > 0) {
                sports=[searchWinnerArray objectAtIndex:indexPath.row];
                
            } else {
                sports=[winnerArray objectAtIndex:indexPath.row];
            }
            
            lblSportsName.text=[NSString stringWithFormat:@"%@",sports.SportName];
            [cell.contentView addSubview:lblSportsName];
            
            UIImageView *imgDetail=[[UIImageView alloc] init];
            imgDetail.image=[UIImage imageNamed:@"right.png"];
            [cell.contentView addSubview:imgDetail];
            
            if (iPad){
                lblSportsName.font = kfontSimple(20);
                lblSportsName.frame = CGRectMake(10, 1, 710, 58);
                imgDetail.frame=CGRectMake(680, 15, 30, 30);
            }else{
                lblSportsName.font = kfontSimple(14);
                lblSportsName.frame = CGRectMake(10, 2, 220, 40);
                imgDetail.frame=CGRectMake(250, 9, 25, 25);
            }
            lblSportsName=nil;
            imgDetail=nil;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"TeamList" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"TeamList"]) {
        
        //the sender is what you pass into the previous method
    }
}

- (IBAction)btnSlideMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(void)dealloc
{
    sports=nil;
    _tbl_winner=nil;
    _searchBar=nil;
    winnerArray=nil;
    searchWinnerArray=nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
