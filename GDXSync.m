//
//  GDXSync.m
//  GameDayXtra
//
//  Created by Darshan Kunjadiya on 23/07/15.
//  Copyright (c) 2015 Darshan Kunjadiya. All rights reserved.
//

#import "GDXSync.h"

GDXSync *_gdxSync = nil;

@implementation GDXSync

+ (id)sharedData
{
    @synchronized(self)
    {
        if (_gdxSync == nil){
            (void)[[self alloc] init]; // assignment not done here
        }
    }
    
    return _gdxSync;
}

//********************************************************************* LOGIN *********************************************************************\\

// Login

#pragma mark - Login Method
-(void)loginMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager POST:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPILogin] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(loginData:error:)])
        {
            [[self delegate] loginData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([[self delegate] respondsToSelector:@selector(loginData:error:)])
        {
            [[self delegate] loginData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}



#pragma mark - SPORT PROFILE METHOD
//********************************************************************* SPORT PROFILE *********************************************************************\\

// Sports View
-(void)getSportsMethod
{
    NSLog(@"%@",[UserDefaults objectForKey:@"AccessToken"]);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //[manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPISport] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getSportsProfileResponseData:error:)])
        {
            [[self delegate] getSportsProfileResponseData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([[self delegate] respondsToSelector:@selector(getSportsProfileResponseData:error:)])
        {
            [[self delegate] getSportsProfileResponseData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];

}

// Club View
-(void)sportClubMethod
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPISportsClub] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getSportsClubResponseData:error:)])
        {
            [[self delegate] getSportsClubResponseData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([[self delegate] respondsToSelector:@selector(getSportsClubResponseData:error:)])
        {
            [[self delegate] getSportsClubResponseData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


// Club Detail
-(void)clubDetailMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIClubDetail] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getClubDetailResponseData:error:)])
        {
            [[self delegate] getClubDetailResponseData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(getClubDetailResponseData:error:)])
        {
            [[self delegate] getClubDetailResponseData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}

// Player Detail
-(void)playerDetailMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIPlayerDetail] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getPlayerDetailResponseData:error:)])
        {
            [[self delegate] getPlayerDetailResponseData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(getPlayerDetailResponseData:error:)])
        {
            [[self delegate] getPlayerDetailResponseData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}
// Squad List
-(void)squadListMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPISquadList] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getSquadListResponseData:error:)])
        {
            [[self delegate] getSquadListResponseData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(getSquadListResponseData:error:)])
        {
            [[self delegate] getSquadListResponseData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


#pragma mark - MY MESSAGE METHOD
//********************************************************************* MY MESSAGE *********************************************************************\\

-(void)myMessagesMethod
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIInBox] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(myMessageResponceData:error:)])
        {
            [[self delegate] myMessageResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(myMessageResponceData:error:)])
        {
            [[self delegate] myMessageResponceData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}
// For Get Outbox Messages
-(void)myOutBoxMethod
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIOutBox] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(myOutBoxResponceData:error:)])
        {
            [[self delegate] myOutBoxResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];

        
        if ([[self delegate] respondsToSelector:@selector(myOutBoxResponceData:error:)])
        {
            [[self delegate] myOutBoxResponceData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


-(void)getFriendList :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIFriendList] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getFriendList:error:)])
        {
            [[self delegate] getFriendList:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];

        
        if ([[self delegate] respondsToSelector:@selector(getFriendList:error:)])
        {
            [[self delegate] getFriendList:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


// For Send messages

-(void)sendMessageMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager POST:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPISendMessage] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(sendMessage:error:)])
        {
            [[self delegate] sendMessage:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(sendMessage:error:)])
        {
            [[self delegate] sendMessage:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


// For Delete messages
-(void)deleteMessageMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIMyMessages] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(deleteMessage:error:)])
        {
            [[self delegate] deleteMessage:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(deleteMessage:error:)])
        {
            [[self delegate] deleteMessage:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


#pragma mark - FAN PROFILE METHOD
//********************************************************************* FAN PROFILE *********************************************************************\\

// My Timeline
-(void)myTimelineMethod
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIMyTimeline] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(myTimelineResponceData:error:)])
        {
            [[self delegate] myTimelineResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(myTimelineResponceData:error:)])
        {
            [[self delegate] myTimelineResponceData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


// Share
-(void)myTimelineShareMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager POST:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPISharePost] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(shareResponceData:error:)])
        {
            [[self delegate] shareResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error ) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(shareResponceData:error:)])
        {
            [[self delegate] shareResponceData:nil error:error];
        }
        NSLog(@"%@",error);
    }];
}

// Like
-(void)postLikeMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIPostLike] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(likeResponceData:error:)])
        {
            [[self delegate] likeResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error ) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(likeResponceData:error:)])
        {
            [[self delegate] likeResponceData:nil error:error];
        }

    }];
}

// Post Comment
-(void)postCommentMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager POST:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIPostComment] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(postCommentResponceData:error:)])
        {
            [[self delegate] postCommentResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(postCommentResponceData:error:)])
        {
            [[self delegate] postCommentResponceData:nil error:error];
        }
        
        NSLog(@"Error  : %@", error);
    }];

}

// Get Comment
-(void)getCommentsMethod :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIGetComments] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(getCommentResponceData:error:)])
        {
            [[self delegate] getCommentResponceData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *ErrorDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options: NSJSONReadingMutableContainers error:nil];
        [_globalGDXData showAlertTitle:kAlertFailed message:[[ErrorDict objectForKey:@"status"] objectForKey:@"message"]];
        
        if ([[self delegate] respondsToSelector:@selector(getCommentResponceData:error:)])
        {
            [[self delegate] getCommentResponceData:nil error:error];
        }
        
        NSLog(@"Error: %@", error);
    }];
}

// Friend Request
-(void)sendFriendRequest :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager POST:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPISendFriendRequest] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(sendFriendRequestData:error:)])
        {
            [[self delegate] sendFriendRequestData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([[self delegate] respondsToSelector:@selector(sendFriendRequestData:error:)])
        {
            [[self delegate] sendFriendRequestData:nil error:error];
        }
        NSLog(@"Error: %@", error);
    }];
}


// Block Friend
-(void)blockFriend :(NSDictionary *)aDictionary
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDefaults objectForKey:@"AccessToken"] forHTTPHeaderField:@"Authorization"];
    [manager GET:[NSString stringWithFormat:@"%@%@",kAPIBASE_URL,kAPIBlockFriend] parameters:aDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[self delegate] respondsToSelector:@selector(blockFriendRequestData:error:)])
        {
            [[self delegate] blockFriendRequestData:responseObject error:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([[self delegate] respondsToSelector:@selector(blockFriendRequestData:error:)])
        {
            [[self delegate] blockFriendRequestData:nil error:error];
        }
        
        NSLog(@"Error: %@", error);
    }];}


@end
