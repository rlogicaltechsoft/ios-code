//
//  GDXSync.h
//  GameDayXtra
//
//  Created by Darshan Kunjadiya on 23/07/15.
//  Copyright (c) 2015 Darshan Kunjadiya. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AppDelegate.h"


@protocol GDXApiDelegate;

@interface GDXSync : NSObject
{
    
}

+ (id)sharedData;

// Login
-(void)loginMethod :(NSDictionary *)aDictionary;

// Sports Profile
-(void)getSportsMethod;// Sports List
-(void)sportClubMethod;// Sports Club list
-(void)clubDetailMethod :(NSDictionary *)aDictionary; // Club Detail
-(void)playerDetailMethod :(NSDictionary *)aDictionary; // Player Detail
-(void)squadListMethod :(NSDictionary *)aDictionary; // Squad List

//MyMessages
-(void)myMessagesMethod; // Inbox
-(void)myOutBoxMethod; // Outbox
-(void)deleteMessageMethod :(NSDictionary *)aDictionary; // For delete Messages
-(void)sendMessageMethod :(NSDictionary *)aDictionary; // For send Message
-(void)getFriendList :(NSDictionary *)aDictionary; // For get Friend list


//Fan Profile
-(void)myTimelineMethod; //  My Timeline
-(void)myTimelineShareMethod :(NSDictionary *)aDictionary; // Share post
-(void)postLikeMethod :(NSDictionary *)aDictionary; // Add Like
-(void)postCommentMethod :(NSDictionary *)aDictionary; // Add comment on Post
-(void)getCommentsMethod :(NSDictionary *)aDictionary; // Get Comment list
-(void)sendFriendRequest :(NSDictionary *)aDictionary; // Send Friend Request
-(void)blockFriend :(NSDictionary *)aDictionary; // Block Friend


@property (nonatomic,weak) id <GDXApiDelegate> delegate;
@end

@protocol GDXApiDelegate <NSObject>

@optional

// Login
-(void)loginData :(NSDictionary *)aLoginDict error:(NSError *)aError; // Login


// Sports Profile
-(void)getSportsProfileResponseData :(NSDictionary *)sportsDict error:(NSError *)aError; // Sports View
-(void)getSportsClubResponseData :(NSDictionary *)sportsClubDict error:(NSError *)aError; // Sports Club View
-(void)getClubDetailResponseData :(NSDictionary *)clubDetailDict error:(NSError *)aError; // Sports Club Detail View
-(void)getPlayerDetailResponseData :(NSDictionary *)playerDetailDict error:(NSError *)aError; // Player Detail View
-(void)getSquadListResponseData :(NSDictionary *)squadListDict error:(NSError *)aError; // Squad List View



//MyMessages
-(void)myMessageResponceData :(NSDictionary *)myMessageDict error:(NSError *)aError;// Inbox
-(void)myOutBoxResponceData :(NSDictionary *)myOutBoxDict error:(NSError *)aError;// Outbox
-(void)deleteMessage :(NSDictionary *)aDeleteDict error:(NSError *)aError; //Delete Message
-(void)sendMessage :(NSDictionary *)aSendDict error:(NSError *)aError; //Send Message
-(void)getFriendList :(NSDictionary *)aFriendDict error:(NSError *)aError; //Get Friend List


//Fan Profile

-(void)myTimelineResponceData :(NSDictionary *)myTimelineDict error:(NSError *)aError ;// My Timeline
-(void)shareResponceData :(NSDictionary *)myShareDict error:(NSError *)aError ;// Post Share
-(void)likeResponceData :(NSDictionary *)likeDict error:(NSError *)aError; // Add Like
-(void)postCommentResponceData :(NSDictionary *)commentDict error:(NSError *)aError ;// Post Comment
-(void)getCommentResponceData :(NSDictionary *)getCommentDict error:(NSError *)aError; // Get Comments
-(void)sendFriendRequestData :(NSDictionary *)aFriendRequestDict error:(NSError *)aError; // Send Friend Request
-(void)blockFriendRequestData :(NSDictionary *)aBlockRequestDict error:(NSError *)aError; // Block Friend

@end
