//
//  LoginViewController.m
//  GameDayXtra
//
//  Created by Macmini on 03/06/15.
//  Copyright (c) 2015 Macmini. All rights reserved.
//

#import "LoginViewController.h"
#import "GlobalGDXData.h"
#import "WinnerViewController.h"



@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize img_user=_img_user;
@synthesize scrlLogin=_scrlLogin;
@synthesize view_middle=_view_middle;
@synthesize txt_email=_txt_email;
@synthesize txt_password=_txt_password;
@synthesize indicator_login=_indicator_login;


- (void)viewDidLoad
{
    if ([_globalGDXData checkStringisNull:[UserDefaults objectForKey:@"userid"]] == TRUE) {
        
        [self setupLoginUIGDX]; // This is for add subviews and set objects frame
    }
    else{
        [self performSegueWithIdentifier:@"Winner" sender:self]; // Pass one view to another view with segue identifier
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)setupLoginUIGDX
{
    
#if TARGET_IPHONE_SIMULATOR
    
    
#endif
    
    setSelectedKeepLogged=FALSE;
    
    // Set image on UIImageView
    self.img_user.image=[UIImage imageNamed:@"no_profile.png"];
    [self.img_user.layer setCornerRadius:15.0f];
    [self.img_user setClipsToBounds:YES];
    // border
    [self.img_user.layer setBorderColor:kColorWhite.CGColor];
    [self.img_user.layer setBorderWidth:5.0f];
    
    // set frame of view_middle and set scrollView contentSize
    if (iPad)
    {
        self.view_middle.frame=CGRectMake(0, 0, 676, 444);
        self.scrlLogin.contentSize=CGSizeMake(288, 400);
    }
    else
    {
        self.view_middle.frame=CGRectMake(0, 0, 288, 247);
        self.scrlLogin.contentSize=CGSizeMake(288, 250);
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    [self.indicator_login stopAnimating];
    self.navigationController.navigationBar.hidden=FALSE;
}


- (IBAction)btn_login:(id)sender
{
    [self.indicator_login startAnimating];
    
    [self.txt_email resignFirstResponder];
    [self.txt_password resignFirstResponder];
    
    if ([_txt_email.text length] != 0 || [_txt_password.text length] != 0){
        
        if (_globalGDXData.internetCheck == YES){
            
            //Valid email address
            if ([_globalGDXData isValidEmail:_txt_email.text] == YES){
                
                [self.indicator_login startAnimating];
                
                _gdxSync.delegate=self;
                [_gdxSync loginMethod:@{@"email_address": [_txt_email.text lowercaseString] , @"password" : _txt_password.text}];
                
            }else{
                
                [self.indicator_login stopAnimating];
                [_globalGDXData showAlertTitle:kAlertError message:kAlertEmailValid];
            }
        }else{
            [self.indicator_login stopAnimating];
        }
    }
    else{
        [self.indicator_login stopAnimating];
        [_globalGDXData showAlertTitle:kAlertFailed message:kAlertEmptyFieldValid];
    }
    
}

-(void)loginData :(NSDictionary *)aLoginDict error:(NSError *)aError
{
    if ([[[aLoginDict objectForKey:@"status"] objectForKey:@"code"] intValue] == kSuccessStatus){
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext){
            
            UserData *userdata = [UserData MR_createEntityInContext:localContext];
            userdata.userid = [NSString stringWithFormat:@"%@",[[aLoginDict objectForKey:@"access"] objectForKey:@"user_id"]];
            userdata.token = [NSString stringWithFormat:@"%@",[[aLoginDict objectForKey:@"access"] objectForKey:@"token"]];
            
        }];
        
        [UserDefaults setObject:[NSString stringWithFormat:@"%@",[[aLoginDict objectForKey:@"access"] objectForKey:@"token"]] forKey:@"AccessToken"];
        [UserDefaults setObject:[NSString stringWithFormat:@"%@",[[aLoginDict objectForKey:@"access"] objectForKey:@"user_id"]] forKey:@"userid"];
        [UserDefaults synchronize];
        
        [self.indicator_login stopAnimating];
        
        NSLog(@"%@",[UserDefaults objectForKey:@"userid"]);
        
        [self performSegueWithIdentifier:@"Winner" sender:self]; // Pass one view to another view with segue identifier
    }else{
        
        [_globalGDXData showAlertTitle:kAlertFailed message:[[aLoginDict objectForKey:@"status"] objectForKey:@"message"]];
    }
    [self.indicator_login stopAnimating];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender // Pass data here to another view form segue identifier
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Winner"])
    {
        
    }
}

- (IBAction)btn_loggeding:(id)sender
{
    if (_ref_loggedin.selected == TRUE){
        
        _ref_loggedin.selected=FALSE;
        setSelectedKeepLogged=FALSE;
    }else{
        
        _ref_loggedin.selected=TRUE;
        setSelectedKeepLogged=TRUE;
    }
}
- (IBAction)btn_CreateAccount:(id)sender
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
